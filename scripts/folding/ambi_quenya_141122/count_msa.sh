for f in `ls *.a3m`
do
    name=$(basename $f | cut -d'.' -f1)
    nb_seqs=$(grep ">" $f|wc -l)
    echo $name $nb_seqs
done
