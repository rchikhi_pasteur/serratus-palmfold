query=$1
#Milot instructions
#mmseqs createdb $query qdb
#mmseqs search qdb target_db res tmp 
#mmseqs result2msa qdb target_db res result_msa.a3m --msa-format-mode 5

mmseqs createdb all.fa qdb
mmseqs search qdb qdb res tmp
mkdir self_res
mv ../res.* .
mv qdb* self_res/
mmseqs result2msa self_res/qdb self_res/qdb self_res/res a3m_self.a3m --msa-format-mode 5

