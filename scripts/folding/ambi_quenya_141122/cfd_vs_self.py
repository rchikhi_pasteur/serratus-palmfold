
from collections import defaultdict
p_cfd=defaultdict(float)
p_self=defaultdict(float)
for (f,p) in [("predictions/log.txt",p_cfd),("self_predictions/log.txt",p_self)]:
    name=None
    plddt=None
    for line in open(f):
        ls = line.split()
        if len(ls) < 3: continue
        if ls[2] == "Query":
            name = ls[4]
        if len(ls) < 4: continue
        if ls[3] == "took":
            assert(ls[8] == "pLDDT")
            plddt=float(ls[9])
            p[name]=max(p[name],plddt)


rdrp=set(list(open("/pasteur/appa/homes/rchikhi/tools/palmfold/pol/rdrp.model.list").read().split()))
xdxp=set(list(open("/pasteur/appa/homes/rchikhi/tools/palmfold/pol/xdxp.model.list").read().split()))
d_cfd = {}
d_self = {}
for (f,d) in [("predictions/palmfold_out_rank_1/all.tm",d_cfd),("self_predictions/palmfold_out/all.tm",d_self)]:
    for line in open(f):
       if line.startswith("PDB"): continue
       if 'rank_1' not in line: continue
       #0_unrelaxed_rank_1_model_1      1hhs_A-cyst     0.1605  0.4315  4.32    0.008   0.029   0.043   472.0   137.0   94.0
       ls = line.split()
       name = ls[0].split("_")[0]
       dist = float(ls[3])
       model=ls[1]
       tophit = "rdrp" if model in rdrp else "xdxp"
       if name not in d:
           d[name] = (dist,tophit)
       else:
           d[name] = max(d[name],(dist,tophit))


msa_cfd = defaultdict(int)
msa_self = defaultdict(int)
for (folder,msa) in [("predictions",msa_cfd), ("self_predictions",msa_self)]:
    """
    import glob
    for filename in glob.glob(folder+"/*.a3m"):
        nb_seqs = 0
        name = filename.split("/")[-1].split(".")[0]
        for line in open(filename):
            if line.startswith(">"):
                nb_seqs += 1
        msa[name]=nb_seqs
    """
    # made using count_msa.sh
    for line in open(folder+"/msas.txt"):
        name, nb_seqs = line.split()
        msa[name]=nb_seqs

print('\t'.join("identifier pLDDT_cfd pLDDT_self TM_cfd tophit_cfd TM_self tophit_self MSA_cfd MSA_self".split()))
for name in d_cfd:
    print(name,p_cfd[name],p_self[name],' '.join(map(str,d_cfd[name])),' '.join(map(str,d_self[name])),msa_cfd[name],msa_self[name],sep='\t')
