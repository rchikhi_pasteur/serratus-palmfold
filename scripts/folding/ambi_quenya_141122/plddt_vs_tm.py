
import sys
if len(sys.argv) > 1:
    input_file = sys.argv[1]
else:
    input_file = "plddt_vs_tm.input.txt"
data = open(input_file).read().split()
log_filename = data[0]
tm_filename = data[1]

name=None
plddt=None
from collections import defaultdict
p=defaultdict(float)
for line in open(log_filename):
    ls = line.split()
    if len(ls) < 3: continue
    if ls[2] == "Query":
        name = ls[4]
    if len(ls) < 4: continue
    if ls[3] == "took":
        assert(ls[8] == "pLDDT")
        plddt=float(ls[9])
        p[name]=max(p[name],plddt)
#print(p)


rdrp=set(list(open("/pasteur/appa/homes/rchikhi/tools/palmfold/pol/rdrp.model.list").read().split()))
xdxp=set(list(open("/pasteur/appa/homes/rchikhi/tools/palmfold/pol/xdxp.model.list").read().split()))

d = dict()
for line in open(tm_filename):
   #0_unrelaxed_rank_1_model_1      1hhs_A-cyst     0.1605  0.4315  4.32    0.008   0.029   0.043   472.0   137.0   94.0
   ls = line.split()
   if ls[0] == "PDBchain1": continue
   name = ls[0].split("_")[0]
   dist = float(ls[3])
   model=ls[1]
   tophit = "rdrp" if model in rdrp else "xdxp"
   if name not in d:
       d[name] = (dist,tophit)
   else:
       d[name] = max(d[name],(dist,tophit))


print("identifier TMscore tophit pLDDT")
for name in d:
	print(name,' '.join(map(str,d[name])),p[name])
