prefix=$(cat prefix)
max=$(find "$prefix"_split/ -type f |wc -l)
find "$prefix"_split_res|grep final.a3m$ |awk -F"/" '{print $2}' |sort -n |uniq > identifiers.txt

seq -s " " -f %02g 0 $max  | tr ' ' '\n' |sort -n> identifiers.all.txt

comm --nocheck-order -13 identifiers.txt identifiers.all.txt > identifiers.remaining.txt
