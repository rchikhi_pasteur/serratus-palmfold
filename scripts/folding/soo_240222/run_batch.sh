#!/bin/bash

set -e

i=$1

# try to get task id, if it's a job array
if [ -z "$i" ]
then
    i=$(printf %02d $SLURM_ARRAY_TASK_ID)
fi

# if all fails..
if [ -z "$i" ]
then
    echo "empty argument"
    exit 1
fi

source $HOME/tools/colabfold/ENV

dbpath=/pasteur/appa/homes/rchikhi/scratch/colabfold_db

#prefix=hmm_E1000_all.fasta_split_res
iprefix=$(cat prefix)
prefix="$iprefix"_split
resprefix="$prefix"_res/$i

mkdir -p "$prefix"_res

rm -Rf "$resprefix"
\time colabfold_search $prefix/$i.fa $dbpath $resprefix
