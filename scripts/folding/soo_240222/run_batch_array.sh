#max=98
#max=3945
prefix=$(cat prefix)
max=$(find "$prefix"_split/ -type f |wc -l)

sbatch --array=0-$max -p common,dedicated -q fast -c 12 --mem 350G --output=slurm-out/%j.out run_batch.sh

