
from collections import defaultdict
names = dict()
counter = 0
for line in open("RNAvirome.S2.fa"):
    if line.startswith('>'):
        name = line[1:].strip()
        names[counter]=name
        counter += 1
d = defaultdict(float)
for line in open("wolf2018-structures.tm"):
   #0_unrelaxed_rank_1_model_1      1hhs_A-cyst     0.1605  0.4315  4.32    0.008   0.029   0.043   472.0   137.0   94.0
   ls = line.split()
   name = ls[0]
   dist = float(ls[3])
   d[name] = max(d[name],dist)

for name in d:
    dist = d[name]
    if dist < 0.6:
        identifier = int(name.split('_')[0])
        print(names[identifier],name,dist,sep='\t')
