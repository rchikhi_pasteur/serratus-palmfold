
import sys
if len(sys.argv) > 1:
    tm_filename = sys.argv[1]

rdrp=set(list(open("/pasteur/appa/homes/rchikhi/tools/palmfold/pol/rdrp.model.list").read().split()))
xdxp=set(list(open("/pasteur/appa/homes/rchikhi/tools/palmfold/pol/xdxp.model.list").read().split()))

d = dict()
for line in open(tm_filename):
   #0_unrelaxed_rank_1_model_1      1hhs_A-cyst     0.1605  0.4315  4.32    0.008   0.029   0.043   472.0   137.0   94.0
   ls = line.split()
   if ls[0] == "PDBchain1": continue
   name = '_'.join(ls[0].split("_")[1:])
   dist = float(ls[3])
   model=ls[1]
   tophit = "rdrp" if model in rdrp else "xdxp"
   if name not in d:
       d[name] = (dist,tophit)
   else:
       d[name] = max(d[name],(dist,tophit))


print("identifier TMscore tophit")
for name in d:
	print(name,' '.join(map(str,d[name])))
