# That script does the postprocessing of step 2 of Daniel DW Kim, described here:
# https://hackseq-rna.slack.com/archives/C02LEPACJDT/p1653214996771419?thread_ts=1652401827.455269&cid=C02LEPACJDT

import sys
filename=sys.argv[1]

sgl = open(filename+".sgl","w")
non_sgl = open(filename+".non_sgl","w")

with open(filename) as f:
    l = f.readline()
    assert(l.startswith('>'))

    l = f.readline()
    assert(l.startswith('>'))
    header = l.strip()[1:]

    while True:
        # invariant: we're at the beginning of a representative sequence
        # next we're gonna read some >'s and stop when we've read 2 consecutive >'s
        l = f.readline()
        ls = l.strip()
        if len(ls) == 0:
            print("unexpected break, there should be a sequence")
            break
        assert(not l.startswith('>'))
        representative_seq = l.strip()

        # skip that next header
        # it's either EOF, or the > for a next sequence, or the 1st > for a next rep
        l = f.readline()
        ls = l.strip()
        if len(ls) == 0:
            break
        assert(l.startswith('>'))

        is_singleton = True

        l = f.readline()
        ls = l.strip()
        if len(ls) == 0:
            break
        while not l.startswith('>'):
            is_singleton = False
            # invariant, we're at the beginning of a sequence
            l = f.readline()
            ls = l.strip()
            if len(ls) == 0:
                break
            assert(l.startswith('>'))
            l = f.readline()
            ls = l.strip()
            if len(ls) == 0:
                break

        assert(len(l.strip()) == 0 or l.startswith('>'))
        # it's the second consecutive > that we read
        # so at this point we're ready to loop 
        if len(l.strip()) > 0:
            next_header = l.strip()[1:]

        if is_singleton:
            #output that rep
            sgl.write(">"+header.replace(" # ","_")+"\n")
            sgl.write(representative_seq+"\n")
        else:
            non_sgl.write(">"+header.replace(" # ","_")+"\n")
            non_sgl.write(representative_seq+"\n")

        header = next_header
        if len(l.strip()) == 0:
            break


