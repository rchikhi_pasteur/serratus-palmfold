#!/bin/bash
#SBATCH --job-name=Serratus-Prodigal
#SBATCH --ntasks=1                     # total number of process
#SBATCH --ntasks-per-node=1            # Number of process per node
#SBATCH --cpus-per-task=80          # nombre de threads OpenMP
#SBATCH --mem=400G
#SBATCH --hint=nomultithread        # on réserve des coeurs physiques et non logiques
#SBATCH --output=logs/%j.out          # nom du fichier de sortie
#SBATCH --error=logs/%j.out         # nom du fichier d'erreur (ici commun avec la sortie)
#SBATCH --time=18:00:00
#SBATCH --array=2-64

cd /pasteur/appa/homes/rchikhi/scratch/serratus_palmfold

srun sh job.unit.sh

