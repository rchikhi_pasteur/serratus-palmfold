import sys
serratusL = dict()
serratusL_seqs = set()
header=None
for line in open("serratusL_rdrp_plus-2022-11-26.fa"):
    line = line.strip()
    if line.startswith(">"):
        header = line[1:]
    else:
        serratusL[header]=line
        serratusL[line]=header
        serratusL_seqs.add(line)

final = dict()
pattern_i = 0
patterns = open("serratusL_rdrp_plus-2022-11-26.seqs_in_rep_final_all_seqs.patterns").readlines()
for i,line in enumerate(open("serratusL_rdrp_plus-2022-11-26.seqs_in_rep_final_all_seqs.fasta")):
    if line.startswith(">"):
        header = line[1:].strip()
    else:
        seq = patterns[pattern_i].strip()
        while seq not in line:
            pattern_i += 1
            seq = patterns[pattern_i].strip()
        if header in final and final[header]  != serratusL[seq]:
            sys.stderr.write(f"weird collision: {serratusL[seq]} != {final[header]}\n")
            exit(1)
        final[header]=serratusL[seq]
    #if i % 100 == 0:
    #    sys.stderr.write(f"{i}..\n")

assoc = dict()
assoc_inv = dict()
import glob
for filename in glob.glob("serratusL-retracing-rdrp_plus-2022-11-26/*.fa"):
    rep = None
    headers = []
    for line in open(filename):
        if line.startswith(">"):
            header = line[1:].strip()
            if header in final:
                rep=final[header]
                assoc[rep]=filename
            headers += [header]
    if rep is not None:
        for header in headers:
            assoc_inv[header]=rep
    else:
        sys.stderr.write(f"{filename} has no rep\n")

g = open("serratusL-retracing-rdrp_plus-2022-11-26.mapping.tsv","w")
for header in assoc:
    print(header,assoc[header],sep='\t',file=g)
g.close

h = open("serratusL-retracing-rdrp_plus-2022-11-26.mapping_inv.tsv","w")
for header in assoc_inv:
    print(header,assoc_inv[header],sep='\t',file=h)
h.close
