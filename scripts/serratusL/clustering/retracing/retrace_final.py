# in double comments: for rdrp_plus
# next to them: for whole clustering
import sys

headers = set()
##for line in open("serratusL_rdrp_plus-2022-11-26.seqs_in_rep_final_all_seqs.fasta"):
for line in open("../rep_final_all_seqs.fasta"):
    if line.startswith(">"):
        header = line[1:].strip()
        headers.add(header)
sys.stderr.write("headers parsed\n")

from collections import defaultdict
final = defaultdict(list)
final_inv = dict()
for lineno,line in enumerate(open("../rep_final_cluster.tsv")):
    ls = line.split()
    rep = ls[0]
    elt = ls[1]
    if rep in headers:
        final[rep] += [elt] 
        final_inv[rep] = rep
        final_inv[elt] = rep
    elif elt in headers:
        sys.stderr.write(f"line {lineno}, unexpected: cluster element in serratusL but not the representative (elt: {elt} rep: {rep})\n")
        #exit(1)
sys.stderr.write("final dict populated\n")

step5_all = set(final_inv)

step3 = defaultdict(list)
for filename in ["../rep_sgl.fa_cluster.tsv","../rep_non_sgl.fa_cluster.tsv"]:
    sys.stderr.write(f"parsing {filename}\n")
    for line in open(filename):
        ls = line.split()
        step3_rep = ls[0]
        step3_elt = ls[1]
        rep = None
        if step3_rep in step5_all:
            rep = final_inv[step3_rep]
            step3[rep] += [step3_elt]
        elif step3_elt in step5_all:
            rep = final_inv[step3_elt]
            step3[rep] += [step3_rep]
            sys.stderr.write("bit of an odd case, step3_rep not found in final clustering but step3_elt is found\n")
sys.stderr.write("step3 parsed\n")

# this file contains all final clusters augmented with sequences from step3
# there are only identifiers, no sequences
##g = open("serratusL_rdrp_plus-2022-11-26.step3_retraced.tsv","w")
g = open("rdva.step3_retraced.tsv","w")
for header in step3:
    res = header + "\t"
    for elt in set(step3[header] + final[header]) - set([header]):
        res += elt + "\t"
    g.write(res+"\n")

g.close()
sys.stderr.write("retraced tsv written\n")
sys.stderr.write("all done!\n")
