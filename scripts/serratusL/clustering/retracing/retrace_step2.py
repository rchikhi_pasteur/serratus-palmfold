# this script retraces the results of step 3 back to step 2
# i.e. it fishes out all the original prodigal genes 
# out of the clustering

import sys
#version="2022-11-26"
version="rdva"

import os
output_folder = f"output-{version}"
if not os.path.exists(output_folder):
    os.mkdir(output_folder)

from collections import defaultdict
#final_clustering = defaultdict(list) #removing to save memory, was unused
final_clustering_keys = set()
final_clustering_inv = defaultdict(list)
#for line in open(f"serratusL_rdrp_plus-{version}.step3_retraced.tsv"):
#for line in open(f"rdva.step3_retraced.tsv"):
filename = sys.argv[1]
for line in open(filename):
    ls = line.split()
    for elt in ls:
        final_clustering_inv[elt] = ls[0]
    #final_clustering[ls[0]] = ls[1:]
    final_clustering_keys.add(ls[0])
sys.stderr.write(f"processed {len(final_clustering_inv)} clustered elements, {len(final_clustering_keys)} reps\n")

def process_cluster(cluster):
    global res,final_clustering_inv
    rep = None
    for (header,seq) in cluster:
        if rep is None:
            #sys.stderr.write(f"testing {header} in {list(final_clustering_inv)[:5]}\n")
            if header in final_clustering_inv:
                rep = final_clustering_inv[header]
        else:
            if header in final_clustering_inv:
                if rep != final_clustering_inv[header]:
                    sys.stderr.write(f"two sequences of a step 2 cluster are clustered differently in step 3:\n")
                    sys.stderr.write(f"header={header} rep={rep}\n")
                    sys.stderr.write(f"final_clustering_inv[header]={final_clustering_inv[header]}\n")
                    #exit(1)
    if rep is not None:
        res[rep].extend(cluster)
        #sys.stderr.write("processed cluster: " + str(cluster) + "\n")
    else:
        sys.stderr.write("couldn't find rep for cluster (incl " + str(cluster[0]) + ")\n")

res = defaultdict(list)
for i in range(1,64):
    sys.stderr.write(f"{i:03}..\n")
    with open(f"../rep_{i:03}_all_seqs.fasta") as f:
        # this big code parses the output of clustering which is a bit weird:
        # >[rep_header]
        # >[rep_header]
        # [rep_seq]
        # >[next_header_in_cluster]
        # [next_seq_in_cluster]
        # >[next_rep_header]
        # >[next_rep_header]
        # [..]

        l = f.readline()
        assert(l.startswith('>'))
        # discard the first header because it's truncated at first space

        l = f.readline()
        ls = l.strip()
        assert(l.startswith('>'))
        header = ls[1:]
        header = header.replace(" # ","_")

        while True:
            # invariant: we're at the beginning of a representative sequence
            # next we're gonna read some >'s and stop when we've read 2 consecutive >'s
            cluster = []
            l = f.readline()
            ls = l.strip()
            if len(ls) == 0:
                sys.stderr.write(f"unexpected break, there should be a sequence (header: {header})\n")
                exit(1)
            assert(not l.startswith('>'))
            rep_header = header
            rep_seq = ls
            cluster += [(rep_header, rep_seq)]

            # skip that next header
            # it's either EOF, or the > for a next sequence, or the 1st > for a next rep
            l = f.readline()
            ls = l.strip()
            if len(ls) == 0:
                break
            assert(ls.startswith('>'))
            header = ls[1:]
            header = header.replace(" # ","_")

            is_singleton = True

            l = f.readline()
            ls = l.strip()
            if len(ls) == 0:
                break
            while not ls.startswith('>'):
                is_singleton = False
                sequence = ls
                cluster += [(header, sequence)]
                # invariant, we're at the beginning of a sequence
                l = f.readline()
                ls = l.strip()
                if len(ls) == 0:
                    break
                assert(ls.startswith('>'))
                header = ls[1:]
                header = header.replace(" # ","_")
                l = f.readline()
                ls = l.strip()
                if len(ls) == 0:
                    break

            assert(len(ls) == 0 or l.startswith('>'))
            # it's either the end of the file or the second consecutive > that we read
            # so at this point we're at the start of a new cluster and ready to loop 
            if len(ls) > 0:
                header = ls[1:]
                header = header.replace(" # ","_")

            if len(ls) == 0:
                break
            process_cluster(cluster)

        # process the last cluster after interrupted loop
        process_cluster(cluster)

# write retraced clusters

counter = 0
for elt in res:
    #g = open(f"{output_folder}/{counter}.fa","w")
    g = open(f"{output_folder}/{os.path.basename(filename)}.{counter}.fa","w")
    for header,seq in res[elt]:
        g.write(f">{header}\n")
        g.write(f"{seq}\n")
    g.close()
    counter += 1

#do some checks

res_keys = set(res.keys())
#final_clustering_keys = set(final_clustering.keys())

for key in res_keys:
    if key not in final_clustering_keys:
        sys.stderr.write(f"warning: {key} in res but not in final_clustering\n")

for key in final_clustering_keys:
    if key not in res_keys:
        sys.stderr.write(f"warning: {key} in final_clustering but not in res\n")

sys.stderr.write("all done!\n")
