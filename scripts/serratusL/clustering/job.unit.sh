taskid=${SLURM_ARRAY_TASK_ID}
taskid=$(printf "%03d" $taskid)

mkdir -p tmp_$taskid
\time mmseqs easy-cluster -c 0.9 --cov-mode 1 --min-seq-id 0.3 split/all_prodigal_prot.part_$taskid.faa rep_$taskid tmp_$taskid

rmdir tmp_$taskid

