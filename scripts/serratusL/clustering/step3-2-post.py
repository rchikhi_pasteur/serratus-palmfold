header=None

good_headers = set()
for line in open("step3-2.non_sgl_headers.txt"):
    line = line.strip()
    good_headers.add(line)

print(len(good_headers),"non-sgl headers read")

g = open("rep_sgl.fa_rep_seq.fasta.non_sgl.fasta","w")

for line in open("rep_sgl.fa_rep_seq.fasta"):
    line=line.strip()
    if line.startswith(">"):
        header=line[1:]
    else:
        if header in good_headers:
            g.write(">"+header+"\n")
            g.write(line+"\n")

g.close()
