#!/bin/bash
find ./rdrp_id90-pdb -type f -name '*_unrelaxed_rank_1_model_*.pdb' | xargs -I{} -P$(nproc) bash -c '
    f="{}"
    file=$(basename "$f")
    dir=$(dirname "$f")
    prefix=$(echo "$file" | awk -F'"[_-]"' "{print \"rdrp_id90_\"\$1\"-\"\$2}")
    mv -- "$f" "$dir/${prefix}.pdb"
'
