#!/bin/bash
BUCKET="s3://palmfold/pdb"

for file in $(grep ambi list_pdb.txt| awk '{print $4}')
do
    newfile="${file/ambi_quenya_/ambiquenya_}"
    aws s3 mv $BUCKET/$file $BUCKET/$newfile
done
