#!/bin/bash

# Function to download and process each file
doit() {
    file=$1
    aws s3 cp s3://serratus-fold/CFDL/$file .  # download
    prefix=$(echo "$file" | cut -d. -f1)
    mkdir -p $prefix
    cd $prefix
    tar -xzf ../$file  # untar
    rm ../$file  # remove the tar.gz file
    # find and extract inner tar.gz files
    find . -name '*.tar.gz' -exec tar -xzf {} \;
    # move all .pdb files to CFDL/ directory
     find . -name '*.pdb' -exec sh -c '
        for file do
            # Extract necessary portions from the file path
            prefix=$(echo $file | cut -d"/" -f2 | sed 's/_/-/')
            
            # Define new filename
            new_filename="CFDL_"$prefix".pdb"
            
            # Rename and move the file
            mv $file /rs/CFDL/$new_filename
        done' sh {} +

    cd ..
    rm -Rf $prefix
}

# Export it so it can be used by GNU Parallel
export -f doit

mkdir -p /rs/CFDL

# Get the list of all files in the S3 bucket and run the function on each
aws s3 ls s3://serratus-fold/CFDL/ | awk '{print $4}' | parallel -j 3 doit
