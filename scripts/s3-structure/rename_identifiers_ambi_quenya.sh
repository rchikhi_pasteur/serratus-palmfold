#!/bin/bash
find predictions -type f -name '*_unrelaxed_rank_1_model_*.pdb*' -exec bash -c '
    for f; do
        file=$(basename "$f")
        dir=$(dirname "$f")
        prefix=$(echo "$file" | cut -d_ -f1)
        mv -- "$f" "ambi_quenya/ambi_quenya_${prefix}.pdb"
    done
' bash {} +
