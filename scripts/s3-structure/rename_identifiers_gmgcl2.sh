#!/bin/bash
find . -type f -name '*.pdb' ! -name 'GMGCL*' -exec bash -c '
    for f; do
        file=$(basename "$f")
        dir=$(dirname "$f")
        number=$(echo "$file" | cut -d. -f1)
        mv -- "$f" "$dir/GMGCL_${number}.pdb"
    done
' bash {} +
