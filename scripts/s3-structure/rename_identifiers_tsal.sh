#!/bin/bash
find TSAL -type f -name '*_unrelaxed_rank_1_model_*.pdb*' -exec bash -c '
    for f; do
        file=$(basename "$f")
        dir=$(dirname "$f")
        prefix=$(echo "$file" | cut -d_ -f1)
        mv -- "$f" "$dir/TSAL_${prefix}.pdb"
    done
' bash {} +
cd ..
