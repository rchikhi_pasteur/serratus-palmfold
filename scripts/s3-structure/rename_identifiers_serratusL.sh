#!/bin/bash

bucket="palmfold"
folder="pdb"

# Use the AWS CLI to list objects in the bucket, then parse the keys
aws s3api list-objects --bucket "${bucket}" --query 'Contents[].{Key: Key}' --prefix "$folder/" --output text | while read -r key
do
    # Use regex to extract X, Y, Z from the filename
    if [[ $key =~ serratusL_([0-9]*)_([0-9]*)_unrelaxed_rank_1_model_([0-9]*).pdb ]]
    then
        x=${BASH_REMATCH[1]}
        y=${BASH_REMATCH[2]}
        z=${BASH_REMATCH[3]}
        
        # Form the new name
        new_key="$folder/serratusL_${x}-${y}.pdb"
        
        # Rename the file by copying it to the new name and deleting the old version
        echo "aws s3api copy-object --copy-source ${bucket}/${key} --bucket ${bucket} --key ${new_key} && aws s3api delete-object --bucket ${bucket} --key ${key}"
    fi
done | parallel -j 50
