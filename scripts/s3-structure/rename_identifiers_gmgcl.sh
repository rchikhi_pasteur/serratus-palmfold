#!/bin/bash
cd gmgcl
find GMGCL -type f -name '*_unrelaxed_rank_1_model_*.pdb*' -exec bash -c '
    for f; do
        file=$(basename "$f")
        dir=$(dirname "$f")
        prefix=$(echo "$file" | cut -d_ -f1)
        mv -- "$f" "$dir/${prefix}.pdb"
    done
' bash {} +
cd ..
