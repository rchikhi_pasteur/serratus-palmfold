mkdir -p rdrp_id90-tm rdrp_id90-pdb rdrp_id90-palmgrab

for folder in `find /rs/id90/untar2 -mindepth 1 -type d` 
do
	#tm=$(ls $folder/*.tm)
	#if [ "$tm" != "" ]
	#then
	#	ln -s $tm rdrp_id90-tm/$(basename $folder)-$(basename $tm)
	#fi
	#pdb=$(ls $folder/*.pdb)
	#if [ "$pdb" != "" ]
	#then
	#	ln -s $pdb rdrp_id90-pdb/$(basename $folder)-$(basename $pdb)
	#fi
	ppfa=$(ls $folder/*.pp.fa)
	if [ "$ppfa" != "" ]
	then
		ln -s $ppfa rdrp_id90-palmgrab/$(basename $folder)-$(basename $ppfa)
	fi
	rcfa=$(ls $folder/*.rc.fa)
	if [ "$rcfa" != "" ]
	then
		ln -s $rcfa rdrp_id90-palmgrab/$(basename $folder)-$(basename $rcfa)
	fi

done

#tar -hzcf /rs/rdrp_id90_all_tm.tar.gz rdrp_id90-tm/
#tar -hcf - rdrp_id90-pdb/ | pigz -p 8 > /rs/rdrp_id90_all_pdb.tar.gz rdrp_id90-pdb/
tar -hzcf /rs/rdrp_id90_all_palmgrab.tar.gz rdrp_id90-palmgrab/
