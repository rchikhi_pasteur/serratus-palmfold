#!/bin/bash

# Define variables
file=$1
num_hits=2

# Read the file and store hit names and maximum TM scores in arrays
mapfile -t hits < <(tail -n +2 $file | awk '{print $2}')
mapfile -t tm_scores < <(tail -n +2 $file | awk '{if ($3 > $4) print $3; else print $4}')

# Find the indices of the top hits based on the maximum score
mapfile -t indices < <(echo "${tm_scores[@]}" | tr ' ' '\n' | cat -n | sort -k2 -rn | head -n $num_hits | awk '{print $1-1}')

# Report the top hits
#echo "Top $num_hits hits based on maximum score over columns 3 and 4:"
for index in "${indices[@]}"
do
    echo "${hits[$index]} ${tm_scores[$index]}"
done
