rm -f summary.tsv
echo -e "filename\tseqname\ta3msize\tmeanplddt\ttop1_tm_name\ttop1_tm_score\ttop2_tm_name\ttop2_tm_score" >> summary.tsv

for folder in `find /rs/id90/untar2 -mindepth 1 -type d` 
do
	a3m=$(ls $folder/*.a3m)
	json=$(ls $folder/*.json)
	tm=$(ls $folder/*.tm)
	if [ "$json" != "" ]
	then
		plddt=$(./average_plddt.sh $json)
	else
		plddt="NA"
	fi
	if [ "$tm" != "" ]
	then
		output=$(./top_2_tm.sh $tm)
		read var1 var2 <<< $(echo "$output" | sed -n 1p)
		read var3 var4 <<< $(echo "$output" | sed -n 2p)
		tmres="$var1\t$var2\t$var3\t$var4"
	else
		tmres="NA\tNA\tNA\tNA"
	fi
	if [ "$a3m" != "" ]
	then
		a3msize=$(grep -c "^>" $a3m)
		name=$(grep "^>" $a3m |head -n 1 |cut -c2-)
		filename=$(basename $folder)
		echo -e "$filename\t$name\t$a3msize\t$plddt\t$tmres" >> summary.tsv
	else
		continue
	fi
done
